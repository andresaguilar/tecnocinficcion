Para Jose David Lopez, Interstellar

¿Qué significa "una hija que es una ley"?

Cuando dices "así comienza posiblemente –para mí–", llenas el texto de incertidumbre. De hecho, la película NO comienza así. Así que el posiblemente debe ir junto al "para mí". Pero esto es innecesario de varias maneras. ¿Estás o no estás seguro de que esta es tu opinión?. Y si es tu opinión, no tienes que decirlo. Podemos suponerlos con la mera existencia de tu texto. Siempre que nos siembres de dudas. 

No estoy de acuerdo que la idea esencial en la película sea la relatividad. Es toda la física gravitacional. La relatividad es solo un efecto dramático. Coincido contigo que el Dr Mann es un personaje central y es, de hecho, quien conduce al desenlace: el rescate!. La historia de amor queda para la secuela nomás. Creo que vas a estar de acuerdo conmigo por la cita del diálogo, que no conversación porque era en situación de stress, entre Cooper y el robot (era TARS?). 

En todo caso, definitivamente creo que te equivocas con la moraleja: 

"También habla de cómo para avanzar se deben dejar cosas atrás, y si es que bien
habla la física acerca de viajes interestelares, también se debe asumir que, tal vez
tratar de modificar el pasado de la historia no sea muy buena idea, sólo queda
aprender de él."

Yo diría que la película muestra no solo que sea posible modificar el pasado, sino incluso que sea indispensable para sobrevivir. 



