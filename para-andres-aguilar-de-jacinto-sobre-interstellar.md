
¿De verdad crees que la película aclara sobre esos conceptos científicos Y los instintos humanos?

Sobre el determinismo histórico: ¿Nuestro destino ya está escrito?, ciertamente la película trae un mensaje para pensar. Pero no creo que sea que nuestra vida se está repitiendo. La película sugiere que podría ser posible comunicarse con el pasado, no viajar hacia él o vivirlo de nuevo. Coop y Murph se encuentran en el futuro de ambos. Es una idea profunda y tiene muchas ramificaciones, incluyendo el papel del Dr. Mann ¿Será que, sabiendo lo que ocurrirá, podemos reprogramar hasta las malas acciones?. 

El resto de tu reflexión me parece muy vaga (ojo, que esto no es malo en sí mismo). Las palabras de la doctora parece ser contrariadas por el propio guión: la gravedad logra trascender el espacio y el tiempo. ¿Será que el amor es una forma de fuerza de gravedad?. 
